# { Mnemosyne }

> My personal infosec memo

[![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-green.svg)](https://gitlab.com/A1CY0N/mnemosyne/-/commits/master)
[![Website https://a1cy0n.gitlab.io/mnemosyne](https://img.shields.io/website-up-down-green-red/http/shields.io.svg)](https://gitlab.com/A1CY0N/mnemosyne)
[![made-with-Go](https://img.shields.io/badge/Made%20with-Go-1f425f.svg)](http://golang.org)

![](public/logo.png)

## Fun fact
The name of this project comes from the Greek goddess of memory: **Mnemosyne**. 
Moreover and regarding our story, it is interesting to note that, it's the origin of the word _memory_.

## Usage

Mnemosyne is publicly available on the sub-domain of the same name of my website. The site is hosted with gitlab pages. You can access with [this link](https://mnemosyne.a1cy0n.xyz/).

## Run the website on your own

To run the website you need first to install [Hugo](https://gohugo.io/getting-started/installing/).

Then, you can clone my repository :

```sh
git clone https://gitlab.com/A1CY0N/mnemosyne.git
```
And run the website with Hugo :

```sh
cd mnemosyne
hugo server --minify --theme book
```
If you want to build static files :

```sh
hugo
```
## Requirements
* Hugo 0.68 or higher
* Hugo extended version, read more [here](https://gohugo.io/news/0.48-relnotes/)
* [Book theme](https://themes.gohugo.io/hugo-book/) for Hugo

## Release History

* 0.1.0
    * First version of the empty site
    * CHANGE: README.md

## Meta

[@A1CY0N](https://mamot.fr/@a1c0n) – a1cy0n@tutanota.com

Distributed under the MIT license. See ``LICENSE`` for more information.

[https://gitlab.com/A1CY0N](https://gitlab.com/A1CY0N)

## Contributing

1. Fork it (<https://gitlab.com/A1CY0N/mnemosyne>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request







