---
title: Introduction
type: docs
---

# My personal cyber memo

{{< columns >}}
## Mnemosyne

_Mnemosyne_ is a character from **Greek mythology**. She is a titanide, daughter of Ouranos and Gaia. Mnemosyne is the **goddess of memory**.

She invented language and words, so that we can express ourselves in an intelligible way. 

<--->

## Why make a site for that?

_Infosec_ being a rather vast domain, this site allows me to organize the different **techniques**, **tools**, **exploits** and **payloads** useful for a cyber engineer.

It's useful for **CTF**, **sys admin**, **pentest**, **osint**, etc...
{{< /columns >}}

## Finding your way in tree structure

| Cheatsheets                                                                  | Resources                                                                    | Training                                                   |
|------------------------------------------------------------------------------|------------------------------------------------------------------------------|------------------------------------------------------------|
| [Android](https://mnemosyne.a1cy0n.xyz/docs/cheatsheet/android/)             | [Exploits](https://mnemosyne.a1cy0n.xyz/docs/resources/exploits/)                                                                     | [OSINT](https://mnemosyne.a1cy0n.xyz/docs/training/osint/) |
| [Cryptography](https://mnemosyne.a1cy0n.xyz/docs/cheatsheet/cryptography/)   | [Formats](https://mnemosyne.a1cy0n.xyz/docs/resources/formats/)                                                                      | CTF                                                        |
| [Linux](https://mnemosyne.a1cy0n.xyz/docs/cheatsheet/linux)                  | [Passwords cracking](https://mnemosyne.a1cy0n.xyz/docs/resources/passwords/) |                                                            |
| [Network](https://mnemosyne.a1cy0n.xyz/docs/cheatsheet/network/)             | [Payloads](https://mnemosyne.a1cy0n.xyz/docs/resources/payloads/)                                                                     |                                                            |
| [Osint](https://mnemosyne.a1cy0n.xyz/docs/cheatsheet/osint/)                                                                        | [Reverse shells](https://mnemosyne.a1cy0n.xyz/docs/resources/reverse_shell/) |                                                            |
| [Pentest](https://mnemosyne.a1cy0n.xyz/docs/cheatsheet/pentest/)                                                                      |                                                                              |                                                            |
| [Phishing](https://mnemosyne.a1cy0n.xyz/docs/cheatsheet/phishing/)                                                                     |                                                                              |                                                            |
| [Steganography](https://mnemosyne.a1cy0n.xyz/docs/cheatsheet/steganography/) |                                                                              |                                                            |
| [Windows](https://mnemosyne.a1cy0n.xyz/docs/cheatsheet/windows/)                                                                      |                                                                              |                                                            |
| [Wireless](https://mnemosyne.a1cy0n.xyz/docs/cheatsheet/wireless/)                                                                     |                                                                              |                                                            |


## Meta

[@A1CY0N](https://mamot.fr/@a1c0n) – a1cy0n@tutanota.com

Distributed under the MIT license. See ``LICENSE`` for more information.

[https://gitlab.com/A1CY0N](https://gitlab.com/A1CY0N)

## Contributing

1. Fork it (<https://gitlab.com/A1CY0N/mnemosyne>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request