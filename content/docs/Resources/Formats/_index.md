---
bookCollapseSection: false
weight: 20
title: "Formats"
---
# Change formats easily

- https://www.rapidtables.com/convert/number/ascii-to-hex.html
- https://gchq.github.io/CyberChef/
- https://www.rapidtables.com/code/text/ascii-table.html
- https://www.base64encode.org/