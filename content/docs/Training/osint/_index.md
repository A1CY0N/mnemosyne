---
bookCollapseSection: false
weight: 20
---
# Platforms for OSINT training

- https://sourcing.games/
- https://ksintel.com/training_osint_exercices/
- https://www.geoguessr.com/
- https://twitter.com/quiztime
- https://ctf.cybersoc.wales/
- https://www.tracelabs.org/