---
weight: 1
bookFlatSection: true
bookCollapseSection: true
title: "Cheatsheet"
---

# Introduction

## Usage

This section groups all cheat sheets sorted by domain. It allows to find easily the command, the payload or the tool to use when you are in a technical context.

## List of technical fields
|  Name |  Description |
|---|---|
| Android       | Android OS security  |
| Cryptography  | Crypto primitives and known weaknesses  |
| Linux         | Linux hardenning & attack  |
| Network       | Networking  |
| OSINT         | Open Sources investigations  |
| Pentest       | Pentest web  |
| Phishing      | Mail scams  |
| Steganography | Steganography methods   |
| Windows       | Windows hardenning & attack  |
| Wireless      | Wireless security  |