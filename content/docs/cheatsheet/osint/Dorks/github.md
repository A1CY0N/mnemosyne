---
bookCollapseSection: false
weight: 20
title: "Github dorks"
---
# Github Dorks
```
https://gist.github.com/jhaddix/77253cea49bf4bd4bfd5d384a37ce7a4

# Github dorks work a lot with filename and extension
# You can build search like this
filename:bashrc
extension:pem
langage:bash

# Possible to search terms and use these keywords
# Some usefull examples
extension:pem private # Private SSH Keys
extension:sql mysql dump # MySQL dumps
extension:sql mysql dump password # MySQL dumps with passwords
filename:wp-config.php # Wordpress config file
filename:.htpasswd # .htpasswd
filename:.git-credentials # Git stored credentials
filename:.bashrc password # .bashrc files containing passwords
filename:.bash_profile aws # AWS keys in .bash_profiles
extension:json mongolab.com # Keys/Credentials for mongolab
HEROKU_API_KEY language:json # Heroku API Keys
HEROKU_API_KEY language:shell # Heroku API Keys
filename:filezilla.xml Pass # FTP credentials
filename:recentservers.xml Pass # FTP credentials
filename:config.php dbpasswd # PHP Applications databases credentials
shodan_api_key language:python # Shodan API Keys (try others languages)
filename:logins.json # Firefox saved password collection (key3.db usually in same repo)
filename:settings.py SECRET_KEY # Django secret keys (usually allows for session hijacking, RCE, etc)
filename:.npmrc _auth
filename:.dockercfg auth
extension:ppk private
filename:id_rsa or filename:id_dsa
filename:credentials aws_access_key_id
filename:.s3cfg
filename:.env DB_USERNAME NOT homestead
filename:.env MAIL_HOST=smtp.gmail.com
PT_TOKEN language:bash
filename:.bashrc mailchimp
rds.amazonaws.com password
extension:json api.forecast.io
extension:yaml mongolab.com
jsforce extension:js conn.login
SF_USERNAME salesforce
filename:.tugboat NOT _tugboat
filename:.netrc password
filename:_netrc password
filename:hub oauth_token
filename:robomongo.json
filename:config.json auths
filename:idea14.key
filename:config irc_pass
filename:connections.xml
filename:express.conf path:.openshift
filename:.pgpass
filename:proftpdpasswd
filename:ventrilo_srv.ini
[WFClient] Password= extension:ica
filename:server.cfg rcon password
JEKYLL_GITHUB_TOKEN
filename:.bash_history
filename:.cshrc
filename:.history
filename:.sh_history
filename:sshd_config
filename:dhcpd.conf
filename:prod.exs NOT prod.secret.exs
filename:prod.secret.exs
filename:configuration.php JConfig password
filename:config.php dbpasswd
filename:config.php pass
path:sites databases password
shodan_api_key language:python
shodan_api_key language:shell
shodan_api_key language:json
shodan_api_key language:ruby
filename:shadow path:etc
filename:passwd path:etc
extension:avastlic "support.avast.com"
filename:dbeaver-data-sources.xml
filename:sftp-config.json
filename:.esmtprc password
extension:json googleusercontent client_secret
HOMEBREW_GITHUB_API_TOKEN language:shell
xoxp OR xoxb
.mlab.com password
filename:logins.json
filename:CCCam.cfg
msg nickserv identify filename:config
filename:settings.py SECRET_KEY
filename:secrets.yml password
filename:master.key path:config
filename:deployment-config.json
filename:.ftpconfig
filename:.remote-sync.json
filename:sftp.json path:.vscode
filename:WebServers.xml
filename:jupyter_notebook_config.json
```