---
bookCollapseSection: false
weight: 20
title: "Google dorks"
---
# Google Dorks

## Easy hacking with google
https://pentest-tools.com/information-gathering/google-hacking

## Search for documents in clouds
```
site:drive.google.com <SEARCH_TERM>
site:dl.dropbox.com <SEARCH_TERM>
site:s3.amazonaws.com <SEARCH_TERM>
site:onedrive.live.com <SEARCH_TERM>
site:cryptome.org <SEARCH_TERM>
```

# Admins credentials
```
intext:COMPANY_KEYWORD & ext:txt | ext:sql | ext:cnf | ext:config | ext:log & intext:"admin" | intext:"root" | intext:"administrator" & intext:"password" | intext:"root" | intext:"admin" | intext:"administrator"

site:http://trello.com intext:company_keyword password + admin OR username
```
# Look for domains indexed by others website
```
site:bgp.he.net inurl:ndd
site:dnslookup.fr inurl:ndd
```

# Get information about a company
```
sites:cadres.apec.fr direction <COMPANY>
site:http://linkedin.com/in "<COMPANY>" (☎ OR ☏ OR ✆ OR 📱) +"<LOCATION>"
```

# Financial reports
```
"périmètre de consolisation"|"rapport de référence"|"rapport annuel" filetype:pdf
```

# When you use the Google Dork:  site:*.example.com, NEVER forget to check
```
site:*.*.example.com
site:*.*.*.example.com
```

# Google Funny dorks
```
site:http://trello.com  site:*/boards
```

# Recon to find sensivite data
```
site:http://ideone.com  | site:http://codebeautify.org  | site:http://codeshare.io  | site:http://codepen.io  | site:http://repl.it  | site:http://justpaste.it  | site:http://pastebin.com  | site:http://jsfiddle.net  | site:http://trello.com  "COMPANY"
```

# Piwik Anonymous Access
```
inurl:token_auth inurl:anonymous
```
# Automated Dorks Tools
```
# GoogD0rker (https://github.com/ZephrFish/GoogD0rker/)
./googD0rker-txt.py -d example.com


# Goohak (https://github.com/1N3/Goohak/
# Just run it on a target domain
./goohak domain.com
```

# Pagodo
```
# Tool to gather dorks information and find potential vulnerable web pages
# The first part is the scrapper which will get dorks and save them
python3 ghdb_scraper.py -j -s

# And then the tool to leverage data and try to find vulnerable pages
# -d option can be used to target a domain
python3 pagodo.py -d example.com -g dorks.txt -l 50 -s -e 35.0 -j 1.1
```