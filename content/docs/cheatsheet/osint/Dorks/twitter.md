---
bookCollapseSection: false
weight: 20
title: "Twitter dorks"
---
# Twitter Dorks

```
# Tweets containing word1 AND word2 (default operator)
<SEARCH_TERM_1> <SEARCH_TERM_2>

# Containing exact expression
"SEARCH_TERM"

# Tweets containing word1 OR word 2
<SEARCH_TERM_1> OR <SEARCH_TERM_2>

# Tweets without word1
-SEARCH_TERM

# Containing "cyber" but without "security"
cyber -security

# All tweets from an account or responding to an account
from:<ACCOUNT>
to:<ACCOUNT>
@ACCOUNT

# Filter results and display only results from followed accounts
filter:follows
exclude:medias

# Tweets after the date or before the date
<SEARCH_TERM> since:2015-02-20
<SEARCH_TERM> until:2015-02-20

# Minimal RT / likes / replies
min_retweets:x
min_faves:x
min_replies:x

# Language
lang:en

# Tweets positive and negative
<SEARCH_TERM> :)
<SEARCH_TERM> :(

# Tweets with a question
<SEARCH_TERM> ?

# Location (city) with a distance range
near:London within:15km

# Tweets corresponding to search term according the filter
# - safe : potentially hard or deleted
# - media : pictures or videos
# - retweets : only retweets
# - native_video : downloaded video (Amplify, Periscope, Vine)
# - periscope 
# - vine
# - images : identified as photos (also coming from Instagram)
# - twimg : pic.twitter.com links
# - links : links to an URL
SEARCH_TERM filter:<filter>
```