---
bookCollapseSection: false
weight: 20
title: "Domain & URLs"
---
# Domain & URLs
## Getting Informations
```
Get information on an IP
whois <IP>

# Get IP address associated to a domain
nslookup domain.fr
ping domain.fr

# Knockknock is a small automated script allowing you to find domain names
# For a registrant (person or company)
python3 k2.py -n company -d

# Many tools
https://intelx.io/

# Estimate the creation date of a website
http://carbondate.cs.odu.edu/
https://github.com/Lazza/Carbon14
```

## Online Passive Identification Tools 
### Target Mapping and Informations
```
# Informations about the target
https://securitytrails.com/

# Robtex is a great and complete tool
https://www.robtex.com

# Getting technology informations
https://builtwith.com

# Mapping the target website can be good to get an overview
www.visualsitemapper.com
```

### Side-domains Identification 
```
# Then you can find domains and subdomains associated to an IP by using Passive Reverse DNS
https://www.virustotal.com/gui/home/search

# RiskIQ Passive DNS

# You can identify new domains in the "RelationShip" section
# of Builtwith
https://builtwith.com/relationships/xxx.fr

# When you have a name or a e-mail adress you can perform a reverse whois lookup to find domain
# names oned by a person of a company
https://viewdns.info/reversewhois/
```
## Scrapping from JS 
```
# You can parse and scrape javascript content in a target website to look for hidden subdomains or interesting paths
# Often, endpoints are not public but users can still interact with them
# Tools like dirscraper automates this (https://github.com/Cillian-Collins/dirscraper)

# Classic
python discraper.py -u <url>

# Output mode
python discraper.py -u <url> -o <output>

# Silent mode (you won't see result in term)
python discraper.py -u <url> -s -o <output>

# Relative URL Extractor is another good tool to scrape from JS files (https://github.com/jobertabma/relative-url-extractor)
ruby extract.rb https://hackerone.com/some-file.js
```

```
# Extract all API endpoints from AngularJS & Angular javascript files
curl -s URL | grep -Po "(\/)((?:[a-zA-Z\-_\:\.0-9\{\}]+))(\/)*((?:[a-zA-Z\-_\:\.0-9\{\}]+))(\/)((?:[a-zA-Z\-_\/\:\.0-9\{\}]+))" | sort -u
```

```
# https://github.com/incogbyte/jsearch
# simple script that grep infos from javascript files
python3 jsearch.py -u https://google.com -n google
```
## Getting page title 
```
# One line
# Getting page title without following redirections
for i in $(cat urls_or_subdomains.txt); do echo "$i | $(curl --connect-timeout 3 $i -s -v 2>&1 | grep -Poz '((?<=title>)(.*)(?=</title>)|(?<=Location:)(.*)/|(Could not resolve host:.*))' | tr -d '\0' | sed -r 's/(https?:\/\/.*\/?)(.*)(301 Moved Permanently)/\3 \2\1/g' )"; done
```
## Investigate a website (crosspost methodology)
```
# Thread by Aware Online about some website investigation methodology
https://twitter.com/aware_online/status/1308312883248467975

1 - Tactical informations: collect and analyze tactical information. 
    A. Visit website
    B. Collect visible info (contact details, VAT numbers, etc.)
    C. Analyze informatione

2 - WHOIS: find owner/host of website
    A. Find top level domain
    B. Find TLD register via http://iana.org/root/domains/db*
    C. Fill in target URL
    D. Look for registrant/registrar

3 - Archives: find archived information
    A. Visit http://archive.org, archive.is or use cached version of search engines
    B. Fill in target URL
    C. Check for archived information

4 - Text: find related information by text
    A. Copy text from target website
    B. Paste text in search engine or use websites such as http://copyscape.com

5 - Reverse Image Search: find websites that use the same/a similar image
    A. Copy image location of target image
    B. Navigate to http://google.com/images or use Yandex, Bing, Baidu, TinEye
    C. Find identical and overeenkomstige afbeeldingen

6 - Images and EXIF data: find EXIF data of digital images
    A. Navigate to target image
    B. Download target image
    C. Extract EXIF data or use Jeffreys Exif viewer / add-ons (wxIF)
    D. Analyze data

7 - Source code: investigate source code
    A. Navigate to target website
    B. View source code
    C. Analyze source code (hidden info, ID's, templates, plugins) Builtwith.com & http://Publicwww.com
    D. Find vulnerabilities/related websites

8 - Others TLD: find other tld's of target website
    A. Use operators -site:http://target.com -site:target.*
    B. Analyze and verify the results

9 - Mentions of target: find mentions of target website
    A. Use operators -site:http://target.com "http://target.com"
    B. Analyze and verify the results

10 - Check infos via RSS: find information via RSS feeds
    A. Navigate to http://target.com/RSS*
    B. Analyze and verify the results

11 - SSL certificates: find (sub)domains of target
    A. Navigate to http://crt.sh
    B. Typ in target website 
    C. Check certificate ID's
    D. Analyze information

12 - Robots/Sitemap: find "hidden" webpages and content
    A. Navigate to http://target.com/robots.txt 
    B. Analyse all disallowed pages and content
    C. Visit pages and content

13 - Port scans: find open ports and services
    A. Use a TCP port scanner such as NMAP
    B. Run scan
    C. Analyze results

14 - Reverse IP lookup: find other domains on same IP address
    A. Use a service such as http://viewdns.info
    B. Typ in target website
    C. Analyze results
    D. Be aware of shared hosting services

15 - Reverse DNS lookup: find other domains that use same DNsame IP address
    A. Use a service such as http://viewdns.info
    B. Typ in target website
    C. Analyze results
    D. Be aware of shared hosting services

16 - Monitoring changes: monitor changes on website
    A. Use a service such as http://visualping.io
    B. Fill in all information
    C. Wait for changes
    D. Analyze changes

17 - Malware check: check for malware on target website
    A. Use a service such as http://any.run
    B. Fill in all information
    C. Wait for the results
    D. Analyze results
```

## r3con1z3r (https://github.com/abdulgaphy/r3con1z3r)

```
# Tools like spoofcheck (https://github.com/BishopFox/spoofcheck) 
# It checks SPF and DMARC records for weak configuration that allow domain spoofing
# Domain is spoofable if lack of an SPF or DMARC record, SPF record never specifies ~all or -all, DMARC policy is set to p=none or is nonexistent
./spoofcheck.py [DOMAIN]
```

## Domain spoofing and typosquatting 
```
# Tools like spoofcheck (https://github.com/BishopFox/spoofcheck) 
# It checks SPF and DMARC records for weak configuration that allow domain spoofing
# Domain is spoofable if lack of an SPF or DMARC record, SPF record never specifies ~all or -all, DMARC policy is set to p=none or is nonexistent
./spoofcheck.py [DOMAIN]
```

```
# urlcrazy allow to generate typo for a given domain and will check different elements
# such as IP, country, Nameserver and MX
https://github.com/urbanadventurer/urlcrazy/

# Default search
urlcrazy domain.com

# You can also search with popularity estimate
urlcrazy -p domain.com
```

## More Information Gathering
```
# EyeWitness (https://github.com/FortyNorthSecurity/EyeWitness)
# It can take screenshots of websites, RDP services and open VNC servers, provide some server header info and identify defualt credentials
./EyeWitness.py -f filename --timeout optionaltimeout --open (Optional)
./EyeWitness -f urls.txt --web
./EyeWitness -x urls.xml --timeout 8 --headless
./EyeWitness -f rdp.txt --rdp
```
```
# XRay tool (https://github.com/evilsocket/xray)
# Bruteforce subdomains using wordlist and DNS requests, then Shodan, then ViewDNS is key is provided.
# Then it will launch banner grabbing and info collectors (not passiv)
xray -shodan-key yadayadayadapicaboo... -viewdns-key foobarsomethingsomething... -domain fbi.gov
```