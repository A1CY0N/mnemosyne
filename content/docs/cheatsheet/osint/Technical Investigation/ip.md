---
bookCollapseSection: false
weight: 20
title: "IP Address"
---
# IP Address
## Getting Informations

### Whois and ASN

```
# Get information on an IP
whois <IP>
```
```
# Enhanced Whois
https://domainbigdata.com
https://whois.domaintools.com
https://www.ip2location.com
https://whoisology.com
```

```
# myip.ms can provie additional informations
https://myip.ms/
```

```
# Get ASN for your target
bgp.he.net
```
```
# Pear is a tool used to get informations about an AS
https://github.com/routeur/Pear
python3 Pear.py -a [ASNumber]
```

```
# You can identify IP block owned by a company
ipv4info.com
```

### Online Scanners
```
# Shodan.io and Censys.io are also usefull
# Keywords on Shodan
https://beta.shodan.io/search/filters
city:"Grenoble"
country:"FR"
geo:"21.452673,56.245212"
server: "gws" hostname:"google"
net:210.214.0.0/16
os:"windows 10"
port:22
```

```
# https://github.com/jakejarvis/awesome-shodan-queries

# Using a premium account, you can search by image
https://images.shodan.io
```

```
# Get information about the target IP
https://www.onyphe.io/

# Robtex is a great and complete tool
https://www.robtex.com
```

```
# Zoomeye can be very efficient, but in Chinese
https://www.zoomeye.org/

# BinaryEdge is also known and powerfull
https://www.binaryedge.io/
```

### Finding real IP behind CloudFlare or Tor

https://www.secjuice.com/finding-real-ips-of-origin-servers-behind-cloudflare-or-tor/

```
# SSL Certificates

# Using a given domain name
# By using an SSL certificate between the server and CloudFlare, you can search for the real server
# Search by "Certificates"
https://censys.io/

# You need to search the certificates for target and show valid ones
parsed.names: xyz123boot.com and tags.raw: trusted

# Then for each result
# Explore > What's using this certificate > IPv4 Hosts

# One of the servers could be the real one
# Does it redirect ?
```

```
# DNS Records

# Even after moving to CloudFlare, some data-driven services still have old A records
# SecurityTrails can do it ("historical data")
https://securitytrails.com/

# MX can also be a good way
```

## Domain Identification
### Dorks
```
# Bing dorks to identify host sharing
ip:xxx.xxx.xxx.xxx
```

### DNSdumpster
```
Find dns records in order to identify the Internet footprint of an organization
https://dnsdumpster.com/
```
### Reverse DNS
```
# Reverse DNS Resolution (also usefull for subdomains)
for x in {1..255}; do host xx.xx.xx.$x; done

# Knockknock is a small automated script allowing you to find domaine names
# For a registrant (person or company)
python3 k2.py -n company -d
```

### Online Services 

```
# Then you can find domains and subdomains associated to an IP by using Passive Reverse DNS
https://www.virustotal.com/gui/home/search

#RiskIQ Passive DNS

# Many passive tests can be done
# Reverse IP Lookup to find new domains
http://www.ipvoid.com/

# When you have a name or a e-mail adress you can perform a reverse whois lookup to find domain
# names oned by a person of a company
https://viewdns.info/reversewhois/

# Many tools (not onlt for IP but like, everything)
https://intelx.io/
https://intelx.io/tools
```